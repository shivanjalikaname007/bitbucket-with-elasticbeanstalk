    Deploy to AWS with Elastic Beanstalk
You can use the Elastic Beanstalk CLI to deploy your application to your Elastic Beanstalk environment. If it's not yet installed on the Docker image that you are using for your pipeline you can simply install it with Python using pip.
pip install awsebcli --upgrade
You may have to install pip as part of your pipeline �  do that by replicating the configuration below which is based on Node.
image: node:7.5.0

    pipelines:
      default:
        - step:
            script:
              - apt-get update && apt-get install -y python-dev
              - curl -O https://bootstrap.pypa.io/get-pip.py
              - python get-pip.py
              - pip install awsebcli --upgrade
Now that the client is installed you just need to initialize your Elastic Beanstalk configuration and run the eb deploy command to deploy your code to your environment of choice. A complete pipeline configuration should look like the following YML.
image: node:7.5.0

    pipelines:
      default:
        - step:
            script: # Modify the commands below to build your repository.
              - npm install
              - npm test
              - apt-get update && apt-get install -y python-dev
              - curl -O https://bootstrap.pypa.io/get-pip.py
              - python get-pip.py
              - pip install awsebcli --upgrade
              - eb init <Application name> -r <AWS region> -p <Application platform>
              - eb deploy <Environment name>